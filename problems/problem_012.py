# Complete the fizzbuzz function to return
# * The word "fizzbuzz" if number is evenly divisible by by both 3 and 5
# * The word "fizz" if number is evenly divisible by only 3
# * The word "buzz" if number is evenly divisible by only 5
# * The number if it is not evenly divisible by 3 nor 5
#
# Try to combine what you have done in the last two problems
# from memory.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.
# --------

# Don't forget -- order is important here!!
# Also there's no mention of whether zero value should be excluded or not

# fizzbuzz --> % 15 == 0 (divisible by 5 and 3 is 15 smallest)
# fizz     --> % 3 == 0
# buzz     --> % 5 == 0


def fizzbuzz(number):
    if number == 0:
        return "let's not do zeros, pick another number"
    elif number % 15 == 0:
        return "fizzbuzz"
    elif number % 3 == 0:
        return "fizz"
    elif number % 5 == 0:
        return "buzz"
    else:
        return number

number = 30

print(fizzbuzz(number))
