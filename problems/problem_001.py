# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


# the function minimum_value is giving us the smaller of two values, value1 and value2
# if one value is greater than the other, display that value
# if it's not, then display the other value

def minimum_value(value1, value2):
    if value1 < value2:
        return value1
    else:
        return value2
