# Complete the max_of_three function so that returns the maximum of three values.
#
# If two values are the same maximum value, return either of them.
# If all of the values are the same, return any of them
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


# -------------------

# Use the >= operator seems like the important thing here
# there are 3 numbers: value1, value2, value3
# we're trying to find the max number
# if the max happens to be tied with 2 other numbers, return either
#     this makes sense because it's the same number
# if all are the same print any -- this also makes sense
# thinking if statement...
# also it says return, not print

# JUNKYARD

#    if value1 >= value2 or value1 >= value3:
#     return value1
#       elif value2 >= value1 or value2 >= value3:
#     return value2
#       elif value3 >= value1 or value3 >= value2:
#     return value3


def max_of_three(value1, value2, value3):
    number_Set = [value1,value2,value3]
    return max(number_Set)

a = 5
b = 5
c = 3

print(max_of_three(a,b,c))


# ANOTHER ONE JUST FOR FUN

def max_of_three(value1, value2, value3):
    if value1 >= value2 and value1 >= value3:
      return value1
    elif value2 >= value1 and value2 >= value3:
        return value2
#    elif value3 >= value1 and value3 >= value2:
    return value3

a = 9
b = 5
c = 55

print(max_of_three(a,b,c))
