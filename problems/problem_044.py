# Complete the translate function which accepts two
# parameters, a list of keys and a dictionary. It returns a
# new list that contains the values of the corresponding
# keys in the dictionary. If the key does not exist, then
# the list should contain a None for that key.
#
# Examples:
#   * keys:       ["name", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     ["Noor", 29]
#   * keys:       ["eye color", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [None, 29]
#   * keys:       ["age", "age", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [29, 29, 29]
#
# Remember that a dictionary has the ".get" method on it.
#---------
# pseudocode
# 1. create an empty list to put stuff in
# 2. for loop to go through each item in the dictionary
# 3. if statement for whether the key is IN that list
# 4. add the value to the list


def translate(key_list, dictionary):
    new_list = []

    for key in key_list:
        new_list.append(dictionary.get(key))

    return new_list


key_list = ["age", "age", "age"]
dictionary = {"name": "Noor", "age": 29}

print(translate(key_list, dictionary))


# SOLUTION

# def translate(key_list, dictionary):
#     result = []                                         # solution
#     for key in key_list:                                # solution
#         result.append(dictionary.get(key))              # solution
#     return result                                       # solution
#     # pass
