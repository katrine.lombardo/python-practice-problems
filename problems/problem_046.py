# Complete the make_sentences function that accepts three
# lists.
#   * subjects contains a list of subjects for three-word sentences
#   * verbs contains a list of verbs for three-word sentences
#   * objects contains a list of objects for three-word sentences
# The make_sentences function should return all possible sentences
# that can be made from the words in each list
#
# Examples:
#   * subjects: ["I"]
#     verbs:    ["play"]
#     objects:  ["Portal"]
#     returns:  ["I play Portal"]
#   * subjects: ["I", "You"]
#     verbs:    ["play"]
#     objects:  ["Portal", "Sable"]
#     returns:  ["I play Portal", "I play Sable",
#                "You play Portal", "You play Sable"]
#   * subjects: ["I", "You"]
#     verbs:    ["play", "watch"]
#     objects:  ["Portal", "Sable"]
#     returns:  ["I play Portal", "I play Sable",
#                "I watch Portal", "I watch Sable",
#                "You play Portal", "You play Sable"
#                "You watch Portal", "You watch Sable"]
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.

def make_sentences(subjects, verbs, objects):
    #created indexes that stay 0 for first 2 nested loops so that
    # verbs and objects can be used multiple times
    verb_index = 0
    obj_index = 0
    # empty list to hold results
    sentence_list = []
    # 3 nested for loops to loops through each argument
    # the only thing changing in subject loop is subject
    for subject in subjects:

        sentence = f"{subject} {verbs[verb_index]} {objects[obj_index]}"
        if sentence not in sentence_list:
            sentence_list.append(sentence)
            print(sentence_list)

        # first nested loop: for each iteration of subject loop, go through list of verbs
        for verb in verbs:
            sentence = f"{subject} {verb} {objects[obj_index]}"
            if sentence not in sentence_list:
                sentence_list.append(sentence)
                print(sentence_list)

            # second nested loop: for each iteration of subject loop and verb loop,
            # go through objects
            for object in objects:
                sentence = f"{subject} {verb} {object}"
                if sentence not in sentence_list:
                    sentence_list.append(sentence)
                    print(sentence_list)

    # make sure the return is located in the right spot
    return sentence_list


subjects = ["I", "You"]
verbs = ["play", "watch"]
objects = ["Portal", "Sable"]

print(make_sentences(subjects, verbs, objects))
