# Complete the reverse_dictionary function which has a
# single parameter that is a dictionary. Return a new
# dictionary that has the original dictionary's values
# for its keys, and the original dictionary's keys for
# its values.
#
# Examples:
#   * input:  {}
#     output: {}
#   * input:  {"key": "value"}
#     output: {"value", "key"}
#   * input:  {"one": 1, "two": 2, "three": 3}
#     output: {1: "one", 2: "two", 3: "three"}

def reverse_dictionary(dictionary): # swap the keys and the values
    new_dictionary = {}

    for key in dictionary:
        new_key = dictionary.get(key) # get fetches the value paired
        # with the key we';'re on in this iteration of the loop

        new_value = key # to get the info you have to loop over the current dictionary
        new_dictionary[new_key] = new_value
        # dictionary_name[what you want the key to be] = what you want value to be
        # d[key] = value

        print(new_dictionary)
    return new_dictionary

print(reverse_dictionary({"one": 1, "two": 2, "three": 3}))

# OR

# def reverse_dictionary(dictionary):
#     new_dictionary = {}

#     for key, value in dictionary.items():
#         new_dictionary[value] = key
#     return new_dictionary


# print(reverse_dictionary({"one": 1, "two": 2, "three": 3}))
