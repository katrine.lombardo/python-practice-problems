# Write a function that meets these requirements.
#
# Name:       group_cities_by_state
# Parameters: a list of cities in the format "«name», «st»"
#             where «name» is the name of the city, followed
#             by a comma and a space, then the two-letter
#             abbreviation of the state
# Returns:    a dictionary whose keys are the two letter
#             abbreviations of the states in the list and
#             whose values are a list of the cities appearing
#             in that list for that state
#
# In the items in the input, there will only be one comma.
#
# Examples:
#     * input:   ["San Antonio, TX"]
#       returns: {"TX": ["San Antonio"]}
#     * input:   ["Springfield, MA", "Boston, MA"]
#       returns: {"MA": ["Springfield", "Boston"]}
#     * input:   ["Cleveland, OH", "Columbus, OH", "Chicago, IL"]
#       returns: {"OH": ["Cleveland", "Columbus"], "IL": ["Chicago"]}
#
# You may want to look up the ".strip()" method for the string.

# ------


def group_cities_by_state(cities):

    # first make an empty dictionary to fill
    new_dictionary = {}

    # let's fill this with states in the keys, and cities in values
    # loop over the original dictionary, getting the keys
    for city in cities: # unknown object that i'm going to assign value to as I loop through it
        print(city)

        name, state = city.split(",") # you know you're getting 2 different variables
                                      # this was a string, is now a list
        print(city.split(","))
        print(name)
        print(state)

        state = state.strip(" ")  # Optional. A set of characters to remove as leading/trailing characters
        print(name, state)

    # append each additional city and state to the new_dictionary
        if state not in new_dictionary:     # check if state is in the new dictionary
            new_dictionary[state] = []      # if not, assign state as the key
        new_dictionary[state].append(name)  # if there's multiple states, it will first test so
                                            # it doesn't make a new key
    return new_dictionary


cities = ["Cleveland, OH", "Columbus, OH", "Chicago, IL"]

print(group_cities_by_state(cities))


# ------------------------------------------------------
# official solution:

def group_cities_by_state(cities):          # solution
    output = {}                             # solution
    for city in cities:                     # solution
        name, state = city.split(",")       # solution
        state = state.strip()               # solution
        if state not in output:             # solution
            output[state] = []              # solution
        output[state].append(name)          # solution
    return output                           # solution

# ------------------------------------------------------

# the strip() function removes spaces at the beginning and at the end of the string:

# Example 1
# Remove spaces at the beginning and at the end of the string:
# txt = "     banana     "
# x = txt.strip()
# print("of all fruits", x, "is my favorite")

# Example 2
# Remove the leading and trailing characters:
# txt = ",,,,,rrttgg.....banana....rrr"
# x = txt.strip(",.grt")
# print(x)

# ------------------------------------------------------

# JUNKYARD

# def group_cities_by_state(cities):

#     # first make an empty dictionary to fill
#     new_dictionary = {}

#     # let's fill this with states in the keys, and cities in values

#     # loop over the original dictionary, getting the keys
#     for city in cities:
#         name, state = city.split(",") # how does this work????


#     # assign states and cities to variables

#     # split the input into 2 lists

#     # after that we can assign one list to the key value and the other to the values.


#     new_dictionary = {
#         "state" : "city"
#     }


#     return new_dictionary

#     # append each additional city and state to the new_dictionary


# cities = ["Cleveland, OH", "Columbus, OH", "Chicago, IL"]

# print(group_cities_by_state(cities))
