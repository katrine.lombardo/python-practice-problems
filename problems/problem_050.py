# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(lst): # list is a preserved keyword so don't make this a variable
    # cut the lst in half
    first_half = []
    second_half = []
    length = len(lst)

    for index in range(length):
        # how do we know if we've reached halfway?
        # check if current index is < halfway point
        if index < (length / 2):
            first_half.append(lst[index])
        else:
            second_half.append(lst[index])
    return first_half, second_half


    # return both lsts
    # if lst length is odd, then extra element lives inside of first lst

lst = [1, 2, 3, 4, 5]

print(halve_the_list(lst))
print(halve_the_list([1, 2, 3, 4]))
print(halve_the_list([1, 2, 3]))





# ANOTHER SOLUTION FROM JORDAN


# def halve_the_list2(lst):
#     list1 = []
#     list2 = []

#     length_list1 = len(lst) // 2 + (len(lst) % 2)

#     for item in range(length_list1):
#         list1.append(lst[item])
#     for item in range(len(lst) // 2):
#         index = item + length_list1
#         list2.append(lst[index])

#     return list1, list2

# lst = [1, 2, 3, 4, 5]

# print(halve_the_list2(lst))
