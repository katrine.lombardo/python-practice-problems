# Write a function that meets these requirements.
#
# Name:       sum_fraction_sequence
# Parameters: a number
# Returns:    the sum of the fractions of the
#             form 1/2+2/3+3/4+...+number/number+1
#
# Examples:
#     * input:   1
#       returns: 1/2
#     * input:   2
#       returns: 1/2 + 2/3
#     * input:   3
#       returns: 1/2 + 2/3 + 3/4


# --------

# pseudocode
    # for number, add all quotients in the sequence frmo 1 to number
    # where 1 is the numerator, and 1+1 is the denominator
    # this pattern continues where numerator increments by 1 each time
    # thus denominator increases by 1 each time also
    # until we reach number / number + 1
    # then we know we're at the end of our fraction sequence
    # so if input number is 5 then output is the result of the following math:
    # 1/2 + 2/3 + 3/4 + 4/5 + 5/6
    # range (1, 6) <1, 2, 3, 4, 5>

def sum_fraction_sequence(number):

    total = 0
    # range(1, number+1) --> range(5) <0, 1, 2, 3, 4> --> because range is NOT inclusive of the last number
    for numerator in range(1, number+1):
        denominator = numerator + 1
        quotient = numerator / denominator
        total += quotient
    return total

number = 5
print(sum_fraction_sequence(number))

# OR


def sum_fraction_sequence2(number):

    fraction_sequence = ""
    for numerator in range(1, number+1):
        if numerator < number:
            fraction_sequence += f"{numerator}/{numerator + 1} + "
        else:
            fraction_sequence += f"{numerator}/{numerator + 1}"

    return fraction_sequence

print(sum_fraction_sequence2(5))





# junkyard
# ----------------------
# from fractions import Fraction

# def sum_fraction_sequence(num):
#     sum = 0                         # create value to iterate on top of to create sum
#     for num in range(1, num+1):     # for where the num is in the range of numbers between 1 and n+1
#         sum += num / (num + 1)      # reassign the value of sum to equal sum + num / (num + 1)



# ----------------------
# from fractions import Fraction


# def sum_fraction_sequence(num):
#     num = num / (num + 1)
#     return Fraction(num)


# print(sum_fraction_sequence(1))
# print(sum_fraction_sequence(2))
# print(sum_fraction_sequence(3))
# print(sum_fraction_sequence(4))
