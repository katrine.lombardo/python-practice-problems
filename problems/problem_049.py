# Write a function that meets these requirements.
#
# Name:       sum_two_numbers
# Parameters: two numerical parameters
# Returns:    the sum of the two numbers
#
# Examples:
#    * x: 3
#      y: 4
#      result: 7

def sum_two_numbers(x,y):
    result = x + y  ##
    return result

x = 5
y = 5

print(sum_two_numbers(x, y))


# random thoughts

# sum takes an iterable (a list or such),
# it doesn’t take two single arguments to sum.
# Because there’s already + for that.
