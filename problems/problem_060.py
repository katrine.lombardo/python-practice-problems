# Write a function that meets these requirements.
#
# Name:       only_odds
# Parameters: a list of numbers
# Returns:    a copy of the list that only includes the
#             odd numbers from the original list
#
# Examples:
#     * input:   [1, 2, 3, 4]
#       returns: [1, 3]
#     * input:   [2, 4, 6, 8]
#       returns: []
#     * input:   [1, 3, 5, 7]
#       returns: [1, 3, 5, 7]

# ------------
# pseudocode

# make a new list to populate with the new copy
# for the old list, evaluate each number
# if the number is not divisible by 2, add to new list
# if not, then move on to the next number
# the new list has even numbers removed
# hint: even numbers are divisible by 2

# ------------



def only_odds(list_of_numbers):
    new_list = []

    for number in list_of_numbers:
        if number % 2 != 0:
            new_list.append(number)
    return new_list


list_of_numbers = [1, 2, 3, 4, 5, 6]

print(only_odds(list_of_numbers))
