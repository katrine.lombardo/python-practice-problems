# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.



attendees_list = 10
members_list = 50


def has_quorum(attendees_list, members_list):
    if attendees_list >= (members_list / 2):
        return True
    return False


print(has_quorum(attendees_list,members_list))
