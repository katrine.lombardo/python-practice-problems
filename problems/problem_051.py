# Write a function that meets these requirements.
#
# Name:       safe_divide
# Parameters: two values, a numerator and a denominator
# Returns:    if the denominator is zero, then returns math.inf.
#             otherwise, returns numerator / denominator
#
# Don't for get to import math!

# -------

# The math module is a standard module in Python and is always available.
# To use mathematical functions under this module, you have to import the
# module using import math.
# It gives access to the underlying C library functions.
# At the top of the function, type --> import math


# The math. inf constant returns a floating-point positive infinity
# print (math.inf) = means print the positive infinity


import math

def safe_divide(numerator, denominator):
    if denominator == 0:
        return math.inf
    else:
        return numerator / denominator


numerator = 0
denominator = 50
print(safe_divide(numerator, denominator))
