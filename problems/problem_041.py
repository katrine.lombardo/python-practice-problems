# Complete the add_csv_lines function which accepts a list
# as its only parameter. Each item in the list is a
# comma-separated string of numbers. The function should
# return a new list with each entry being the corresponding
# sum of the numbers in the comma-separated string.
#
# These kinds of strings are called CSV strings, or comma-
# sepearted values strings.
#
# Examples:
#   * input:  []
#     output: []
#   * input:  ["3", "1,9"]
#     output: [3, 10]
#   * input:  ["8,1,7", "10,10,10", "1,2,3"]
#     output:  [16, 30, 6]
#
# Look up the string split function to find out how to
# split a string into pieces.

# Write out your own pseudocode to help guide you.

def add_csv_lines(csv_lines):
    # there is only one parameter, list
    # we need to make a new empty list to populate
    new_list = []

    # now we run a for loop to run through each item in the list
    for item in csv_lines:

        # we'll use the split function here to separate each indexed item
        pieces = item.split(",") # define pieces here so we can use this keyword instead
        line_sum = 0             # make it empty to add each individual item

        # now we start a new for loop for the items in each list
        # to convert each number in the string list into an integer
        for piece in pieces: # nested loop
            value = int(piece)
            line_sum += value # it's going to add this each time to my new list

        new_list.append(line_sum) # the sum here is doing the math
        print(new_list)

    return new_list


csv_lines = ["8,1,7", "10,10,10", "1,2,3,4,5"]

print(add_csv_lines(csv_lines))


# OR
# def add_csv_lines(csv_lines):
#     new_list = []
#     for item in csv_lines:
#         count = 0
#         item = item.split(",")
#         for num in item:
#             num = int(num)
#             count += num
#         new_list.append(count)
#     return new_list

# print(add_csv_lines(["2,2,2", "3,3,3"]))




# split-string method

# Syntax
# string.split(separator, maxsplit)
# Parameter Values
# Parameter	Description
# separator	Optional. Specifies the separator to use when splitting the string. By default any whitespace is a separator
# maxsplit	Optional. Specifies how many splits to do. Default value is -1, which is "all occurrences"

# examples
# txt = "hello, my name is Peter, I am 26 years old"
# x = txt.split(", ")
# print(x)

# txt = "apple#banana#cherry#orange"
# # setting the maxsplit parameter to 1, will return a list with 2 elements
# x = txt.split("#", 1)
# print(x)
