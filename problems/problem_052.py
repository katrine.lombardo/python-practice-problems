# Write a function that meets these requirements.
#
# Name:       generate_lottery_numbers
# Parameters: none
# Returns:    a list of six random unique numbers
#             between 1 and 40, inclusive
#
# Example bad results:
#    [4, 2, 3, 3, 1, 5] duplicate numbers
#    [1, 2, 3, 4, 5] not six numbers
#
# You can use randint from random, here, or any of
# the other applicable functions from the random
# package.
#
# https://docs.python.org/3/library/random.html

import random

def generate_lottery_numbers():
    random_list = []

    while len(random_list) < 6:
        random_integer = random.randint(1,40) # random_integer = random.randint(0,40) # create a random integer
        if random_integer not in random_list:
            random_list.append(random_integer)
    return random_list

print(generate_lottery_numbers())



# ------------

# ANOTHER SOLUTION

# import random

# def generate_lottery_numbers():
#     #takes a random sample of 6 digits between 1 and 40
#     return random.sample(range(1, 40), 6)

# print(generate_lottery_numbers())

# ------------

# junkyard

# import random

# def generate_lottery_numbers():
#     random_list = list(range(0,7))
#     random_integer = random.randint(0,7) # create a random integer

#     for i in range(len(random_list)):



#     return random_list # 6 random unique numbers between 1 and 40 (so between 0 and 41)

# print(generate_lottery_numbers())
