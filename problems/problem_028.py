# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

#old_string = "abccbad"


def remove_duplicate_letters(old_string):
    new_string = ""
    for letter in old_string:
        if letter not in new_string:
            new_string += letter

    return new_string


old_string = "hello"

print(remove_duplicate_letters(old_string))




# boulevard of broken dreams

# def remove_duplicate_letters(s):
#     if len(s) == 0:
#         return None
#     else:
#         for char in s:
#             if char ==
#             # if the character in the first place(0 index in a list, which is not this)
#             # is equal to the character in the next place
#             # remove that character


# print(remove_duplicate_letters("hullo"))
