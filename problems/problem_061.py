# Write a function that meets these requirements.
#
# Name:       remove_duplicates
# Parameters: a list of values
# Returns:    a copy of the list removing all
#             duplicate values and keeping the
#             original order
#
# Examples:
#     * input:   [1, 1, 1, 1]
#       returns: [1]
#     * input:   [1, 2, 2, 1]
#       returns: [1, 2]
#     * input:   [1, 3, 3, 20, 3, 2, 2]
#       returns: [1, 3, 20, 2]

# pseudocode

# make a new list to populate with the new copy
# for the old list, evaluate each number
# if the number is already in list, then continue
# if the number does not exist in the new list, append it to the new list
# the new list has duplicates removed


def remove_duplicates(list_of_values):
    new_list = []
    for number in list_of_values:
        if number in new_list:
            continue
        else:
            new_list.append(number)
    return new_list

list_of_values = [1, 3, 3, 20, 3, 2, 2]
print(remove_duplicates(list_of_values))
