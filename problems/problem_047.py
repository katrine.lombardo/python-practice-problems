# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    # password is a str
    # make an empty list, loop through

    has_lowercase = False
    has_uppercase = False
    has_digit = False
    has_special = False
    special_characters = ["$","!","@"]

    # if len(password) >= 6 and len(password) <= 12:

    for character in password:
        if character.islower():
            has_lowercase = True
        if character.isupper():
            has_uppercase = True
        if character.isdigit():
            has_digit = True
        if character in special_characters:
            has_special = True

    return (
        has_lowercase and
        has_uppercase and
        has_digit and
        has_special and
        len(password) >= 6 and
        len(password) <= 12
    )

password = "t!@T333333333"

print(check_password(password))



# junkyard


    # if len(password) >= 6 and len(password) <= 12:
    #     return True

    # for character in password:
    #     if character.isalpha():
    #         return True
    #     if character.isdigit():
    #         return True
    #     if character.
    #     else:
    #         return False


# -------


    # if len(password) >= 6 and len(password) <= 12:
    #     True

    # for character in password:
    #     if (
    #         character.islower() or
    #         character.isupper() or
    #         character.isdigit() or
    #         character == "$" or "!"" or "@"
    #     )
    #         return True
    # return True



# -----------

def check_password(password):
    # password is a str
    # make an empty list, loop through

    has_lowercase = False
    has_uppercase = False
    has_digit = False
    has_special = False

    # if len(password) >= 6 and len(password) <= 12:

    for character in password:
        if character.islower():
            has_lowercase.append(character)
        if character.isupper():
            has_uppercase.append(character)
        if character.isdigit():
            has_digit.append(character)
        if character == "$" or "!" or "@":
            has_special.append(character)

    return (
        has_lowercase and
        has_uppercase and
        has_digit and
        has_special and
        len(password) >= 6 and len(password) <= 12
    )

password = "testing123@"

print(check_password(password))
