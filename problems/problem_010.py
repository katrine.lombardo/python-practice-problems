# Complete the is_divisible_by_3 function to return the
# word "fizz" if the value in the number parameter is
# divisible by 3. Otherwise, just return the number.
#
# You can use the test number % 3 == 0 to test if a
# number is divisible by 3.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.
# ---------

# if there is NOT a remainder, return the word "Fizz"
# if there is a remainder, return the number
# we should use modulo for this one

# had some trouble understanding "return" in the instructions
# but i figured it out (almost) by myself!

def is_divisible_by_3(number):
    if number % 3 == 0:
        return "Fizz"
    else:
        return number

number = 16

print(is_divisible_by_3(number))
