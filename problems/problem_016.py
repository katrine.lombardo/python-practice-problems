# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.




def is_inside_bounds(x, y):
    if (x >= 0 and x < 11) and (y >=0 and y <11):
        return "coordinates are in bounds"
    else:
        return "out of bounds"

coordinate_x = 10
coordinate_y = 10

print(is_inside_bounds(coordinate_x,coordinate_y))
