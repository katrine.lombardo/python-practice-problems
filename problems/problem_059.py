# Write a function that meets these requirements.
#
# Name:       specific_random
# Parameters: none
# Returns:    a random number between 10 and 500, inclusive,
#             that is divisible by 5 and 7
#
# Examples:
#     * returns: 35
#     * returns: 105
#     * returns: 70
#
# Guidance:
#   * Generate all the numbers that are divisible by 5
#     and 7 into a list
#   * Use random.choice to select one


# ---------
# pseudocode

# make list for numbers to go into
# generate all numbers divisible by 5 and 7
# enter those numbers into the list
# use random.choice to select one

import random

def specific_random():

    number_list = range(10,501)
    divisible_list = []

    for number in number_list:
        if number % 5 == 0 and number % 7 == 0:
            divisible_list.append(number)
    print(divisible_list)
    return random.choice(divisible_list)


print(specific_random())
