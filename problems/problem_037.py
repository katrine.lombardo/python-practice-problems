# Complete the pad_left function which takes three parameters
#   * a number
#   * the number of characters in the result
#   * a padding character
# and turns the number into a string of the desired length
# by adding the padding character to the left of it
#
# Examples:
#   * number: 10
#     length: 4
#     pad:    "*"
#     result: "**10"
#   * number: 10
#     length: 5
#     pad:    "0"
#     result: "00010"
#   * number: 1000
#     length: 3
#     pad:    "0"
#     result: "1000"
#   * number: 19
#     length: 5
#     pad:    " "
#     result: "   19"

def pad_left(number, length, pad):
    string = "" # create empty string to fill
    number = str(number) # converting number to string to count the # of characters
                         # because len doesn't work with an integer!!!!

    for index in range(length - len(number)): # use index to remind we're counting it
        string += pad    # this gets you the right number of pad characters
    return string + number

number = 487
length = 7
pad = "*"

print(pad_left(number,length,pad))
