# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# values = [5, 50, 3, 10, 9]

def find_second_largest(values):
    values = []
    if len(values) < 2:
        return None

    reversed_values = sorted(values,reverse=True)
    return reversed_values[1]

values = [5, 50, 3, 10, 9]
print(find_second_largest([5, 50, 3, 10, 9]))


# Katrine -- make this work
