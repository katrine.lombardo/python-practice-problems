# Complete the pairwise_add function which accepts two lists
# of the same size. It creates a new list and populates it
# with the sum of corresponding entries in the two lists.
#
# Examples:
#   * list1:  [1, 2, 3, 4]
#     list2:  [4, 5, 6, 7]
#     result: [5, 7, 9, 11]
#   * list1:  [100, 200, 300]
#     list2:  [ 10,   1, 180]
#     result: [110, 201, 480]
#
# Look up the zip function to help you with this problem.
# -------

# pseudocode
# 1. create a new empty list
# 2. for loop to iterate over for each tuple
# 3. do the zip function. Note: zipped = list(zip(list1+list2)) is the same as zipped = list(zip(list1,list2)))
# 4. append the sum to the empty list

def pairwise_add(list1, list2):
    new_list = []

    for value1, value2 in zip(list1,list2):
        new_list.append(value1 + value2)

    return new_list

list1 =  [100, 200, 300]
list2 =  [ 11,   1, 180]

print(pairwise_add(list1,list2))


# ---------
# The zip() function returns a zip object, which is an
# iterator of tuples where the first item in each passed
# iterator is paired together, and then the second item in
# each passed iterator are paired together etc.

# ex):
# a = ("John", "Charles", "Mike")
# b = ("Jenny", "Christy", "Monica", "Vicky")
# x = zip(a, b)
# output --> (('John', 'Jenny'), ('Charles', 'Christy'), ('Mike', 'Monica'))
