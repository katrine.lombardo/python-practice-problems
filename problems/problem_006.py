# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.
# --------

# we have to set up age as an integer variable
# if statement with age >= 18
# someone can skydive only if both of two conditions are met = has


# Are you old enough to skydive?
age = 16

# Do you have a signed consent form?
has_consent_form = True


def can_skydive(age, has_consent_form):
    if (has_consent_form == True) or (age >= 18):
        print("Eligible")
    else:
        print("Not Eligible")

can_skydive(age, has_consent_form)






# JUNKYARD

# def can_skydive(age, has_consent_form):
#     if has_consent_form = "Y" and age >= 18:
#         print("Eligible")
#     print("Not Eligible")

# age = 18
# has_consent_form = "Y"
